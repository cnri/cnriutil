/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.
               
        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
         http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.cnri.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TransferQueue;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

/** 
 * A ThreadPoolExecutor which will prefer to add threads (up to a specified maximum pool size) before queueing tasks.
 * It works by using a {@code LinkedTransferQueue} which calls {@code tryTransfer} instead of 
 * {@code offer} when the ThreadPoolExecutor considers queuing a task; if a new thread cannot be spawned,
 * a wrapper for the ThreadPoolExecutor's {@code RejectedExecutionHandler} puts the task on the queue.
 */
public class GrowBeforeTransferQueueThreadPoolExecutor extends ThreadPoolExecutor {
    
    private volatile RejectedExecutionHandler handler;
    private final TransferQueue<Runnable> workQueue;
 
    /**
     * Constructs a GrowBeforeTransferQueueThreadPoolExecutor based on an unbounded {@code LinkedTransferQueue}.
     */
    public GrowBeforeTransferQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, new LinkedTransferQueue<>());
    }

    /**
     * Constructs a GrowBeforeTransferQueueThreadPoolExecutor based on an unbounded {@code LinkedTransferQueue} using the given {@code ThreadFactory}.
     */
    public GrowBeforeTransferQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, ThreadFactory threadFactory) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, new LinkedTransferQueue<>(), threadFactory);
    }

    public GrowBeforeTransferQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, TransferQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new TransferOnOfferWrapper(workQueue));
        this.workQueue = workQueue;
        this.handler = super.getRejectedExecutionHandler();
        super.setRejectedExecutionHandler(new EnqueueUnlessShutdownRejectedExecutionHandlerWrapper(this.handler));
    }

    public GrowBeforeTransferQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, TransferQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new TransferOnOfferWrapper(workQueue), threadFactory);
        this.workQueue = workQueue;
        this.handler = super.getRejectedExecutionHandler();
        super.setRejectedExecutionHandler(new EnqueueUnlessShutdownRejectedExecutionHandlerWrapper(this.handler));
    }

    public GrowBeforeTransferQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, TransferQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new TransferOnOfferWrapper(workQueue), new EnqueueUnlessShutdownRejectedExecutionHandlerWrapper(handler));
        this.workQueue = workQueue;
        this.handler = handler;
    }

    public GrowBeforeTransferQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, TransferQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new TransferOnOfferWrapper(workQueue), threadFactory, new EnqueueUnlessShutdownRejectedExecutionHandlerWrapper(handler));
        this.workQueue = workQueue;
        this.handler = handler;
    }

    @Override
    public TransferQueue<Runnable> getQueue() {
        return workQueue;
    }
    
    @Override
    public RejectedExecutionHandler getRejectedExecutionHandler() {
        return handler;
    }

    @Override
    public void setRejectedExecutionHandler(RejectedExecutionHandler handler) {
        if (handler == null) throw new NullPointerException();
        this.handler = handler;
        super.setRejectedExecutionHandler(new EnqueueUnlessShutdownRejectedExecutionHandlerWrapper(handler));
    }

    private static class EnqueueUnlessShutdownRejectedExecutionHandlerWrapper implements RejectedExecutionHandler {
        private final RejectedExecutionHandler delegate;

        public EnqueueUnlessShutdownRejectedExecutionHandlerWrapper(RejectedExecutionHandler delegate) {
            this.delegate = delegate;
        }

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            if (executor.isShutdown()) {
                delegate.rejectedExecution(r, executor);
                return;
            } else {
                // non-shutdown rejection always means failure to spawn a new thread
                try {
                    executor.getQueue().put(r);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
    
    private static class TransferOnOfferWrapper implements TransferQueue<Runnable> {
        private final TransferQueue<Runnable> delegate;
        
        public TransferOnOfferWrapper(TransferQueue<Runnable> delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean offer(Runnable e) {
            return delegate.tryTransfer(e);
        }

        @Override public void forEach(Consumer<? super Runnable> action) { delegate.forEach(action); }
        @Override public boolean tryTransfer(Runnable e) { return delegate.tryTransfer(e); }
        @Override public void transfer(Runnable e) throws InterruptedException { delegate.transfer(e); }
        @Override public boolean tryTransfer(Runnable e, long timeout, TimeUnit unit) throws InterruptedException { return delegate.tryTransfer(e, timeout, unit); }
        @Override public boolean hasWaitingConsumer() { return delegate.hasWaitingConsumer(); }
        @Override public int getWaitingConsumerCount() { return delegate.getWaitingConsumerCount(); }
        @Override public boolean add(Runnable e) { return delegate.add(e); }
        @Override public int size() { return delegate.size(); }
        @Override public boolean isEmpty() { return delegate.isEmpty(); }
        @Override public Runnable remove() { return delegate.remove(); }
        @Override public Runnable poll() { return delegate.poll(); }
        @Override public Runnable element() { return delegate.element(); }
        @Override public Iterator<Runnable> iterator() { return delegate.iterator(); }
        @Override public Runnable peek() { return delegate.peek(); }
        @Override public void put(Runnable e) throws InterruptedException { delegate.put(e); }
        @Override public Object[] toArray() { return delegate.toArray(); }
        @Override public boolean offer(Runnable e, long timeout, TimeUnit unit) throws InterruptedException { return delegate.offer(e, timeout, unit); }
        @Override public <T> T[] toArray(T[] a) { return delegate.toArray(a); }
        @Override public Runnable take() throws InterruptedException { return delegate.take(); }
        @Override public Runnable poll(long timeout, TimeUnit unit) throws InterruptedException { return delegate.poll(timeout, unit); }
        @Override public int remainingCapacity() { return delegate.remainingCapacity(); }
        @Override public boolean remove(Object o) { return delegate.remove(o); }
        @Override public boolean contains(Object o) { return delegate.contains(o); }
        @Override public int drainTo(Collection<? super Runnable> c) { return delegate.drainTo(c); }
        @Override public int drainTo(Collection<? super Runnable> c, int maxElements) { return delegate.drainTo(c, maxElements); }
        @Override public boolean containsAll(Collection<?> c) { return delegate.containsAll(c); }
        @Override public boolean addAll(Collection<? extends Runnable> c) { return delegate.addAll(c); }
        @Override public boolean removeAll(Collection<?> c) { return delegate.removeAll(c); }
        @Override public boolean removeIf(Predicate<? super Runnable> filter) { return delegate.removeIf(filter); }
        @Override public boolean retainAll(Collection<?> c) { return delegate.retainAll(c); }
        @Override public void clear() { delegate.clear(); }
        @Override public boolean equals(Object o) { return delegate.equals(o); }
        @Override public int hashCode() { return delegate.hashCode(); }
        @Override public Spliterator<Runnable> spliterator() { return delegate.spliterator(); }
        @Override public Stream<Runnable> stream() { return delegate.stream(); }
        @Override public Stream<Runnable> parallelStream() { return delegate.parallelStream(); }
        @Override public String toString() { return delegate.toString(); }
    }
}
