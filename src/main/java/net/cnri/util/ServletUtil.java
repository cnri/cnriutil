/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.
               
        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
         http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.cnri.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

//import javax.servlet.http.HttpServletRequest;

abstract public class ServletUtil {
    private ServletUtil() {}

//    public static String undecodedContext(HttpServletRequest req) {
//        return pathMatching(req.getRequestURI(),req.getContextPath());
//    }

//    public static String undecodedPath(HttpServletRequest req) {
//        return pathExcluding(req.getRequestURI(),req.getContextPath());
//    }

    public static String pathMatching(String full, String prefix) {
        return pathMatching(full,prefix,true);
    }

    public static String pathExcluding(String full, String prefix) {
        return pathExcluding(full,prefix,true);
    }

    public static String pathMatching(String full, String prefix, boolean prefixMayBeEncoded) {
        if (full.startsWith(prefix)) {
            String suffix = full.substring(prefix.length());
            if (suffix.isEmpty() || suffix.startsWith("/") || suffix.startsWith("%2F") || suffix.startsWith("%2f")) {
                return prefix;
            }
        }
        
        int slashes = countSlashesOrEncodedSlashes(prefix,prefixMayBeEncoded);
        int index = indexOfNthSlashOrEncodedSlash(full,slashes+1);
        String prefixInFull;
        if(index >= 0) prefixInFull = full.substring(0,index);
        else prefixInFull = full;
        if(urlDecode(prefixInFull).equals(urlDecode(prefix))) {
            return prefixInFull;
        } else return prefix;
    }
    
    public static String pathExcluding(String full, String prefix, boolean prefixMayBeEncoded) {
        if (full.startsWith(prefix)) {
            String suffix = full.substring(prefix.length());
            if (suffix.isEmpty() || suffix.startsWith("/") || suffix.startsWith("%2F") || suffix.startsWith("%2f")) {
                return suffix;
            }
        }
        
        int slashes = countSlashesOrEncodedSlashes(prefix,prefixMayBeEncoded);
        int index = indexOfNthSlashOrEncodedSlash(full,slashes+1);
        String prefixInFull;
        if(index >= 0) prefixInFull = full.substring(0,index);
        else prefixInFull = full;
        if(urlDecode(prefixInFull).equals(urlDecode(prefix))) {
            if(index < 0) return "";
            else return full.substring(index);
        } else return full;
    }

    public static int indexOfNextSlashOrEncodedSlash(String string, int start) {
        int slashIndex = string.indexOf('/',start);
        int twoEffIndex = string.indexOf("%2F",start);
        if(slashIndex < 0 || (0 <= twoEffIndex && twoEffIndex < slashIndex)) slashIndex = twoEffIndex;
        twoEffIndex = string.indexOf("%2f",start);
        if(slashIndex < 0 || (0 <= twoEffIndex && twoEffIndex < slashIndex)) slashIndex = twoEffIndex;
        return slashIndex;
    }

    public static int countSlashesOrEncodedSlashes(String string, boolean stringMayBeEncoded) {
        int count = 0;
        int lastIndex = -1;
        while(true) {
            int nextIndex = stringMayBeEncoded ? indexOfNextSlashOrEncodedSlash(string,lastIndex+1) : string.indexOf('/',lastIndex+1);
            if(nextIndex<0) return count;
            count++;
            lastIndex = nextIndex;
        }
    }
    
    public static int indexOfNthSlashOrEncodedSlash(String string,int n) {
        int lastIndex = -1;
        for(int i = 0; i < n; i++) {
            lastIndex = indexOfNextSlashOrEncodedSlash(string,lastIndex+1);
            if(lastIndex < 0) return i - n;  // -k means found all but k
        }
        return lastIndex;
    }
    
    public static String urlDecode(String s) {
        return StringUtils.decodeURLIgnorePlus(s);
    }
    
    public static boolean isThisMyIpAddress(String addrString) {
        InetAddress addr;
        try {
            addr = InetAddress.getByName(addrString);
        }
        catch(UnknownHostException e) {
            return false;
        }
        // Check if the address is a valid special local or loop back
        if (addr.isLoopbackAddress() || addr.isAnyLocalAddress())
            return true;

        // Check if the address is defined on any interface
        try {
            return NetworkInterface.getByInetAddress(addr) != null;
        } catch (SocketException e) {
            return false;
        }
    }
}
