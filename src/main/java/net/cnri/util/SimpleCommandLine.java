/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.
               
        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
         http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.cnri.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Simple parser for command-line options.  A {@code SimpleCommandLine} object is contructed knowing which options require arguments, and whether to allow grouping of 
 * single-character options (that is, whether "-help" is one option, or equivalent to "-h -e -l -p").  Grouping is off by default.
 * <p>
 * After construction, call {@link #parse(String[])} on the array of command-line arguments.  Then use the other methods to access options,
 * option arguments, and operands.  The object can only be used to parse arguments once.
 * <p>
 * The parser will treat "-" as an operand, and "--" as a special delimiter indicating that all remaining arguments are operands even if they being with "-".
 * Option arguments can be separated from the option with either a space or an equals sign.  If single-character grouping is on, single-character options
 * can be immediately before their arguments, with "-oarg", "-o=arg", and "-o arg" all equivalent.  There is no distinction made between "-o" and "--o",
 * or, with grouping off, between "-long" and "--long".  Optional (non-required) option arguments are supported only by the equals sign---an option with an equals sign 
 * will be counted as indicating an option argument even if the option was not listed in the options requiring arguments.  The class does not in any way show an
 * equivalence if there are multiple ways to express an option, so "-v", "--ver", and "--version" will look like three different options called "v", "ver", and "version". 
 * 
 * Prefer jopt-simple
 */
public class SimpleCommandLine {
    private final boolean groupSingleCharacterOptions;
    private final List<String> optionsExpectingArguments;
    
    private List<String> options = new ArrayList<>();
    private Map<String, List<String>> optionArguments = new LinkedHashMap<>();
    private List<String> operands = new ArrayList<>();
    private boolean parsed;
    
    /**
     * Constructs a {@code SimpleCommandLine} instance expecting the specified options to require arguments, with no grouping of single-character options.
     * 
     * @param optionsExpectingArguments the options requiring arguments, given without initial hyphens
     */
    public SimpleCommandLine(String... optionsExpectingArguments) {
        this(false, optionsExpectingArguments);
    }

    /**
     * Constructs a {@code SimpleCommandLine} instance expecting the specified options to require arguments, with no grouping of single-character options.
     * 
     * @param optionsExpectingArguments the options requiring arguments, given without initial hyphens
     */
    public SimpleCommandLine(List<String> optionsExpectingArguments) {
        this(false, optionsExpectingArguments.toArray(new String[optionsExpectingArguments.size()]));
    }
    
    /**
     * Constructs a {@code SimpleCommandLine} instance expecting the specified options to require arguments and potentially allowing grouping of single-character options.
     * 
     * @param groupSingleCharacterOptions whether to allow grouping of single-character options
     * @param optionsExpectingArguments the options requiring arguments, given without initial hyphens
     */
    public SimpleCommandLine(boolean groupSingleCharacterOptions, List<String> optionsExpectingArguments) {
        this(groupSingleCharacterOptions, optionsExpectingArguments.toArray(new String[optionsExpectingArguments.size()]));
    }
        
    /**
     * Constructs a {@code SimpleCommandLine} instance expecting the specified options to require arguments and potentially allowing grouping of single-character options.
     * 
     * @param groupSingleCharacterOptions whether to allow grouping of single-character options
     * @param optionsExpectingArguments the options requiring arguments, given without initial hyphens
     */
    public SimpleCommandLine(boolean groupSingleCharacterOptions, String... optionsExpectingArguments) {
        this.groupSingleCharacterOptions = groupSingleCharacterOptions;
        this.optionsExpectingArguments = Arrays.asList(optionsExpectingArguments);
    }
    
    private void putOptionArgument(String option, String optionArg) {
        List<String> optionArgs = optionArguments.get(option);
        if (optionArgs == null) {
            optionArgs = new ArrayList<>();
            optionArguments.put(option, optionArgs);
        }
        optionArgs.add(optionArg);
    }
    
    /**
     * Parses the command-line arguments.  Can only be called once and must be called before any other methods.
     * 
     * @param args the array of command-line arguments
     */
    public void parse(String[] args) {
        if (parsed) throw new IllegalStateException();
        boolean sawDoubleDash = false;
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (sawDoubleDash) {
                operands.add(arg);
            } else if ("--".equals(arg)) {
                sawDoubleDash = true;
            } else if (arg.equals("-") || !arg.startsWith("-")) {
                operands.add(arg);
            } else {
                String nextArg = null;
                if (i + 1 < args.length) nextArg = args[i+1];
                boolean skipNextArg = parseOptionReturnTrueIfSkipNextArg(arg, nextArg);
                if (skipNextArg) i++;
            }
        }
        preventFurtherModification();
        parsed = true;
    }

    private void preventFurtherModification() {
        options = Collections.unmodifiableList(options);
        operands = Collections.unmodifiableList(operands);
        for (Map.Entry<String, List<String>> entry : optionArguments.entrySet()) {
            optionArguments.put(entry.getKey(), Collections.unmodifiableList(entry.getValue()));
        }
        optionArguments = Collections.unmodifiableMap(optionArguments);
    }

    private boolean parseOptionReturnTrueIfSkipNextArg(String arg, String nextArg) {
        boolean twoDashes = arg.startsWith("--");
        if (twoDashes || !groupSingleCharacterOptions) {
            return parseMultiCharOptionReturnTrueIfSkipNextArg(arg, nextArg);
        } else {
            return parseGroupedSingleCharOptionsReturnTrueIfSkipNextArg(arg, nextArg);
        }
    }

    private boolean parseGroupedSingleCharOptionsReturnTrueIfSkipNextArg(String arg, String nextArg) {
        String prevOption = null;
        // start at 1 to skip hyphen
        for (int j = 1; j < arg.length(); j++) {
            String thisOption = arg.substring(j, j+1);
            if (prevOption != null && thisOption.equals("=")) {
                putOptionArgument(prevOption, arg.substring(j+1));
                return false;
            }
            options.add(thisOption);
            prevOption = thisOption;
            if (optionsExpectingArguments.contains(thisOption)) {
                if (j + 1 == arg.length()) {
                    if (nextArg != null) {
                        putOptionArgument(thisOption, nextArg);
                        return true;
                    }
                } else {
                    String optionArg;
                    if (arg.charAt(j+1) == '=') {
                        optionArg = arg.substring(j+2);
                    } else {
                        optionArg = arg.substring(j+1);
                    }
                    putOptionArgument(thisOption, optionArg);
                    return false;
                }
            }
        }
        return false;
    }

    private boolean parseMultiCharOptionReturnTrueIfSkipNextArg(String arg, String nextArg) {
        boolean twoDashes = arg.startsWith("--");
        String option = arg.substring(twoDashes ? 2 : 1);
        int equalSign = option.indexOf('=', 1);  // allow first character to be equal sign
        String optionArg = null;
        boolean skipNextArg = false;
        if (equalSign > 0) {
            optionArg = option.substring(equalSign + 1);
            option = option.substring(0, equalSign);
        } else if (nextArg != null && optionsExpectingArguments.contains(option)) {
            optionArg = nextArg;
            skipNextArg = true;
        }
        options.add(option);
        if (optionArg != null) {
            putOptionArgument(option, optionArg);
        }
        return skipNextArg;
    }
    
    /**
     * Returns whether an option was included (with or without option-argument).
     * 
     * @param option the option, without initial hyphens
     * @return whether the option was included
     */
    public boolean hasOption(String option) {
        if (!parsed) throw new IllegalStateException();
        return options.contains(option);
    }

    /**
     * Returns a list of all included options.
     * 
     * @return the list of all included options
     */
    public List<String> getOptions() {
        if (!parsed) throw new IllegalStateException();
        return options;
    }

    /**
     * Returns the argument for the given option.  If the option occurred multiple times, returns the last argument.
     * 
     * @param option the option, without initial hyphenes
     * @return the option argument
     */
    public String getOptionArgument(String option) {
        if (!parsed) throw new IllegalStateException();
        List<String> args = optionArguments.get(option);
        if (args == null || args.isEmpty()) return null;
        return args.get(args.size() - 1);
    }
    
    /**
     * Returns all arguments which appeared for the given option.  For example after parsing "-x=1 -x=2", {@code getOptionArguments("x")}
     * will return a two-element list containing the strings "1" and "2".
     * 
     * @param option the option, without initial hyphenes
     * @return a list of the option arguments
     */
    public List<String> getOptionArguments(String option) {
        if (!parsed) throw new IllegalStateException();
        return optionArguments.get(option);
    }

    /**
     * Returns a map of all option arguments.
     * 
     * @return a map of all option arguments
     */
    public Map<String, List<String>> getAllOptionArguments() {
        if (!parsed) throw new IllegalStateException();
        return optionArguments;
    }
    
    /**
     * Returns the list of operands (arguments which were not options nor option-arguments).
     * 
     * @return the list of operands
     */
    public List<String> getOperands() {
        if (!parsed) throw new IllegalStateException();
        return operands;
    }
}
