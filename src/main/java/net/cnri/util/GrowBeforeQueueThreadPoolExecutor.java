/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.
               
        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
         http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.cnri.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/** 
 * A ThreadPoolExecutor which will prefer to add threads (up to a specified maximum pool size) before queueing tasks.
 * It works by, after a task is queued, executing {@link #SENTINEL_NO_OP} which can not be enqueued (by this class's special wrapped queue)
 * and so will cause a new thread to be spawned, if possible.
 * Instead of {@link #beforeExecute(Thread,Runnable)} and {@link #afterExecute(Runnable,Throwable)}, subclasses will need to implement
 * {@link #beforeExecuteIgnoreSentinel(Thread,Runnable)} and {@link #afterExecuteIgnoreSentinel(Runnable,Throwable)} instead; these
 * methods will never be called on the sentinel.
 * 
 * Prefer the simpler {@link GrowBeforeTransferQueueThreadPoolExecutor}.
 */
public class GrowBeforeQueueThreadPoolExecutor extends ThreadPoolExecutor {
    /** Sentinel runnable used internally to encourage new threads to spawn.  This is just <code>new Runnable() { public void run() { } }</code> */
    public static final Runnable SENTINEL_NO_OP = new Runnable() { @Override public void run() { } };
    
    private final BlockingQueue<Runnable> workQueue;
    private final SentinelRejectingBlockingQueueWrapper queueWrapper;
    private volatile RejectedExecutionHandler handler;
    private final AtomicLong completedSentinelCount = new AtomicLong();
    private final AtomicLong completedTaskCount = new AtomicLong();
    
    public GrowBeforeQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new SentinelRejectingBlockingQueueWrapper(workQueue));
        this.queueWrapper = (SentinelRejectingBlockingQueueWrapper)super.getQueue();
        this.workQueue = workQueue;
        this.handler = super.getRejectedExecutionHandler();
        super.setRejectedExecutionHandler(new SentinelIgnoringRejectedExecutionHandlerWrapper(this.handler));
    }

    public GrowBeforeQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new SentinelRejectingBlockingQueueWrapper(workQueue), threadFactory);
        this.queueWrapper = (SentinelRejectingBlockingQueueWrapper)super.getQueue();
        this.workQueue = workQueue;
        this.handler = super.getRejectedExecutionHandler();
        super.setRejectedExecutionHandler(new SentinelIgnoringRejectedExecutionHandlerWrapper(this.handler));
    }

    public GrowBeforeQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new SentinelRejectingBlockingQueueWrapper(workQueue), new SentinelIgnoringRejectedExecutionHandlerWrapper(handler));
        this.queueWrapper = (SentinelRejectingBlockingQueueWrapper)super.getQueue();
        this.workQueue = workQueue;
        this.handler = handler;
    }

    public GrowBeforeQueueThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new SentinelRejectingBlockingQueueWrapper(workQueue), threadFactory, new SentinelIgnoringRejectedExecutionHandlerWrapper(handler));
        this.queueWrapper = (SentinelRejectingBlockingQueueWrapper)super.getQueue();
        this.workQueue = workQueue;
        this.handler = handler;
    }
    
    @Override
    public void execute(Runnable command) {
        super.execute(command);
        if (!queueWrapper.enoughWaiters()) {
            super.execute(SENTINEL_NO_OP);
        }
    }

    /**
     * Marked as final, so can no longer be overridden; override {@link #beforeExecuteIgnoreSentinel(Thread,Runnable)} instead.
     */
    @Override
    protected final void beforeExecute(Thread t, Runnable r) {
        if (r == SENTINEL_NO_OP) return;
        beforeExecuteIgnoreSentinel(t, r);
    }

    /**
     * Method invoked prior to executing the given Runnable in the
     * given thread.  This method is invoked by thread {@code t} that
     * will execute task {@code r}, and may be used to re-initialize
     * ThreadLocals, or to perform logging.
     *
     * <p>This implementation does nothing, but may be customized in
     * subclasses. Note: To properly nest multiple overridings, subclasses
     * should generally invoke {@code super.beforeExecuteIgnoreSentinel} at the end of
     * this method.
     *
     * @param t the thread that will run task {@code r}
     * @param r the task that will be executed
     */
    protected void beforeExecuteIgnoreSentinel(Thread t, Runnable r) { }

    /**
     * Marked as final, so can no longer be overridden; override {@link #afterExecuteIgnoreSentinel(Runnable,Throwable)} instead.
     */
    @Override
    protected final void afterExecute(Runnable r, Throwable t) {
        if (r == SENTINEL_NO_OP) completedSentinelCount.incrementAndGet();
        afterExecuteIgnoreSentinel(r, t);
    }
    
    /**
     * Method invoked upon completion of execution of the given Runnable.
     * This method is invoked by the thread that executed the task. If
     * non-null, the Throwable is the uncaught {@code RuntimeException}
     * or {@code Error} that caused execution to terminate abruptly.
     *
     * <p>This implementation does nothing, but may be customized in
     * subclasses. Note: To properly nest multiple overridings, subclasses
     * should generally invoke {@code super.afterExecuteIgnoreSentinel} at the
     * beginning of this method.
     *
     * <p><b>Note:</b> When actions are enclosed in tasks (such as
     * {@link java.util.concurrent.FutureTask}) either explicitly or via methods such as
     * {@code submit}, these task objects catch and maintain
     * computational exceptions, and so they do not cause abrupt
     * termination, and the internal exceptions are <em>not</em>
     * passed to this method. If you would like to trap both kinds of
     * failures in this method, you can further probe for such cases,
     * as in this sample subclass that prints either the direct cause
     * or the underlying exception if a task has been aborted:
     *
     *  <pre> {@code
     * class ExtendedExecutor extends ThreadPoolExecutor {
     *   // ...
     *   protected void afterExecuteIgnoreSentinel(Runnable r, Throwable t) {
     *     super.afterExecuteIgnoreSentinel(r, t);
     *     if (t == null && r instanceof Future<?>) {
     *       try {
     *         Object result = ((Future<?>) r).get();
     *       } catch (CancellationException ce) {
     *           t = ce;
     *       } catch (ExecutionException ee) {
     *           t = ee.getCause();
     *       } catch (InterruptedException ie) {
     *           Thread.currentThread().interrupt(); // ignore/reset
     *       }
     *     }
     *     if (t != null)
     *       System.out.println(t);
     *   }
     * }}</pre>
     *
     * @param r the runnable that has completed
     * @param t the exception that caused termination, or null if
     * execution completed normally
     */
    @SuppressWarnings("javadoc")
    protected void afterExecuteIgnoreSentinel(Runnable r, Throwable t) { }

    @Override
    public long getTaskCount() {
        long sentinelCount = completedSentinelCount.get();
        return super.getTaskCount() - sentinelCount;
    }

    @Override
    public long getCompletedTaskCount() {
        long sentinelCount = completedSentinelCount.get();
        long superCompletedTaskCount = super.getCompletedTaskCount();
        long res = superCompletedTaskCount - sentinelCount;
        long previousRes = completedTaskCount.get();
        if (res < previousRes) return previousRes;
        else {
            completedTaskCount.compareAndSet(previousRes, res);
            return completedTaskCount.get();
        }
    }
    
    @Override
    public BlockingQueue<Runnable> getQueue() {
        return workQueue;
    }

    @Override
    public RejectedExecutionHandler getRejectedExecutionHandler() {
        return handler;
    }

    @Override
    public void setRejectedExecutionHandler(RejectedExecutionHandler handler) {
        if (handler == null) throw new NullPointerException();
        this.handler = handler;
        super.setRejectedExecutionHandler(new SentinelIgnoringRejectedExecutionHandlerWrapper(handler));
    }

    private static class SentinelIgnoringRejectedExecutionHandlerWrapper implements RejectedExecutionHandler {
        private final RejectedExecutionHandler delegate;

        public SentinelIgnoringRejectedExecutionHandlerWrapper(RejectedExecutionHandler delegate) {
            this.delegate = delegate;
        }

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            if (r == SENTINEL_NO_OP) return;
            delegate.rejectedExecution(r, executor);
        }
    }
    
    private static class SentinelRejectingBlockingQueueWrapper implements BlockingQueue<Runnable> {
        private final BlockingQueue<Runnable> delegate;
        private final AtomicInteger waiters = new AtomicInteger();
        
        public SentinelRejectingBlockingQueueWrapper(BlockingQueue<Runnable> delegate) {
            this.delegate = delegate;
        }

        public boolean enoughWaiters() {
            int size = delegate.size();
            if (size == 0) return true;
            return waiters.get() >= size;
        }

        @Override
        public boolean offer(Runnable e) {
            if (e == SENTINEL_NO_OP) {
                return enoughWaiters();
            }
            return delegate.offer(e);
        }
        
        @Override
        public boolean add(Runnable e) {
            return delegate.add(e);
        }

        @Override
        public boolean addAll(Collection<? extends Runnable> c) {
            return delegate.addAll(c);
        }

        @Override
        public void clear() {
            delegate.clear();
        }

        @Override
        public boolean contains(Object o) {
            return delegate.contains(o);
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return delegate.containsAll(c);
        }

        @Override
        public int drainTo(Collection<? super Runnable> c, int maxElements) {
            return delegate.drainTo(c, maxElements);
        }

        @Override
        public int drainTo(Collection<? super Runnable> c) {
            return delegate.drainTo(c);
        }

        @Override
        public Runnable element() {
            return delegate.element();
        }

        @Override
        public boolean isEmpty() {
            return delegate.isEmpty();
        }

        @Override
        public Iterator<Runnable> iterator() {
            return delegate.iterator();
        }

        @Override
        public boolean offer(Runnable e, long timeout, TimeUnit unit) throws InterruptedException {
            return delegate.offer(e, timeout, unit);
        }

        @Override
        public Runnable peek() {
            return delegate.peek();
        }

        @Override
        public Runnable poll() {
            return delegate.poll();
        }

        @Override
        public Runnable poll(long timeout, TimeUnit unit) throws InterruptedException {
            try {
                waiters.incrementAndGet();
                return delegate.poll(timeout, unit);
            } finally {
                waiters.decrementAndGet();
            }
        }

        @Override
        public void put(Runnable e) throws InterruptedException {
            delegate.put(e);
        }

        @Override
        public int remainingCapacity() {
            return delegate.remainingCapacity();
        }

        @Override
        public Runnable remove() {
            return delegate.remove();
        }

        @Override
        public boolean remove(Object o) {
            return delegate.remove(o);
        }

        @Override
        public boolean removeAll(Collection<?> arg0) {
            return delegate.removeAll(arg0);
        }

        @Override
        public boolean retainAll(Collection<?> arg0) {
            return delegate.retainAll(arg0);
        }

        @Override
        public int size() {
            return delegate.size();
        }

        @Override
        public Runnable take() throws InterruptedException {
            try {
                waiters.incrementAndGet();
                return delegate.take();
            } finally {
                waiters.decrementAndGet();
            }
        }

        @Override
        public Object[] toArray() {
            return delegate.toArray();
        }

        @Override
        public <T> T[] toArray(T[] arg0) {
            return delegate.toArray(arg0);
        }
        
        
    }
    
}
